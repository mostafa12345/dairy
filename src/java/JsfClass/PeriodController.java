package JsfClass;

import Entity.Period;
import Entity.Saham;
import Entity.Users;
import JsfClass.util.JsfUtil;
import JsfClass.util.JsfUtil.PersistAction;
import SessionBean.PeriodFacade;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("periodController")
@SessionScoped
public class PeriodController implements Serializable {

    @EJB
    private SessionBean.PeriodFacade ejbFacade;
    @EJB
    private SessionBean.SahamFacade sahamFacade;
    private List<Period> items = null;
    private Period selected;
    private String newCount;

    public String getNewCount() {
        return newCount;
    }

    public void setNewCount(String newCount) {
        this.newCount = newCount;
    }

    public Period getBase() {
        return ejbFacade.getBase();
    }

    public Period getNow() {
        return ejbFacade.getNow();
    }

    public PeriodController() {
    }

    public Period getSelected() {
        return selected;
    }

    public void setSelected(Period selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private PeriodFacade getFacade() {
        return ejbFacade;
    }

    public Period prepareCreate() {
        selected = new Period();
        initializeEmbeddableKey();
        return selected;
    }
    private int NotEquals = 0;

    public int getNotEquals() {
        return NotEquals;
    }

    public void setNotEquals(int NotEquals) {
        this.NotEquals = NotEquals;
    }
    private int addSaham;

    public int getAddSaham() {
        return addSaham;
    }

    public void setAddSaham(int addSaham) {
        this.addSaham = addSaham;
    }

    private Users user;

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public void addSahamToUser() {
        System.out.println("User Is ==> " + user.getName() + " add Count is ==> " + addSaham);
        if (addSaham <= NotEquals) {
            Saham newSaham = new Saham();
            newSaham.setCount(addSaham);
            newSaham.setSerialNo(String.valueOf(Integer.valueOf(sahamFacade.getLastSerialNo().getSerialNo()) + 1));
            newSaham.setStatus(1);
            newSaham.setUserId(user);
            newSaham.setDateRegister(new Date());
            NotEquals = NotEquals - addSaham;
            sahamFacade.create(newSaham);
            this.selected = ejbFacade.getNow();
            selected.setNotEquale(NotEquals);
            JsfUtil.addSuccessMessage("این سهام افزوده شد مقدار " + NotEquals + " باقی مانده است");
            getFacade().edit(selected);
        } else {
            JsfUtil.addErrorMessage("مقدار وارد شده صحیح نمی باشد");
        }
    }
    
    public boolean checkNotEquale(){
        selected = ejbFacade.getNow();
        if(selected != null && selected.getNotEquale() != null && selected.getNotEquale() > 0){
            this.NotEquals = selected.getNotEquale();
            return true;
        }
        return false;
    }
    public void create() {
        Integer oldCount = sahamFacade.getCountActiveSaham();
        if ((ejbFacade.count() == 1 && oldCount == 45701) || ejbFacade.count() > 1) {
            if (!newCount.isEmpty()) {
                float sharesOffer = ((Float.valueOf(newCount)) / oldCount);
                String helpShare = String.valueOf(sharesOffer);
                helpShare = helpShare.length() > 4 ? helpShare.substring(0, 4) : helpShare;
                sharesOffer = Float.valueOf(helpShare);
                selected.setNewCount(Integer.valueOf(this.newCount));
                selected.setOldCount(oldCount);
                selected.setSharesOffer(sharesOffer);
                this.NotEquals = (int) (Integer.valueOf(newCount) - (oldCount * sharesOffer));
                selected.setNotEquale(NotEquals);
//                System.out.println("Old Count is : " + oldCount + " ,newCount : " + newCount + " ,ShareOffer is : " + sharesOffer + " ,shareoffer * oldCount is : " + (sharesOffer * oldCount));
            }
            if (oldCount > 1) {
                selected.setLastSerialNo(Integer.valueOf(sahamFacade.getLastSerialNo().getSerialNo()));
            }
            if (selected.getIsBase() == true) {
                ejbFacade.setNewBases();
            } else {
                selected.setIsNow(true);
            }
            if (selected.getIsBase() == false && !JsfUtil.isValidationFailed()) {
                ejbFacade.setNewNows();
            }
            persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle8").getString("PeriodCreated"));

            if (!JsfUtil.isValidationFailed()) {
                items = null;    // Invalidate list of items to trigger re-query.
            }
        }
    }

    public void update() {
        if (selected.getIsBase() == true) {
            ejbFacade.setNewBases();
        }
        if (selected.getIsNow() == true) {
            ejbFacade.setNewNows();
        }
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle8").getString("PeriodUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle8").getString("PeriodDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Period> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle8").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle8").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Period getPeriod(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Period> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Period> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Period.class)
    public static class PeriodControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            PeriodController controller = (PeriodController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "periodController");
            return controller.getPeriod(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Period) {
                Period o = (Period) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Period.class.getName()});
                return null;
            }
        }

    }

}

package JsfClass;

import Entity.MeetingDetail;
import JsfClass.util.JsfUtil;
import JsfClass.util.JsfUtil.PersistAction;
import SessionBean.MeetingDetailFacade;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.UploadedFile;

@Named("meetingDetailController")
@SessionScoped
public class MeetingDetailController implements Serializable {

    @EJB
    private SessionBean.MeetingDetailFacade ejbFacade;
    private List<MeetingDetail> items = null;
    private MeetingDetail selected;
    private UploadedFile file;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public MeetingDetailController() {
    }

    public MeetingDetail getSelected() {
        return selected;
    }

    public void setSelected(MeetingDetail selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private MeetingDetailFacade getFacade() {
        return ejbFacade;
    }

    public MeetingDetail prepareCreate() {
        selected = new MeetingDetail();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() throws IOException {
        String Extension = FilenameUtils.getExtension(file.getFileName());
        if(Extension !=""){
            String fileName = "";
            File dir = new File("/opt/saham/meetingDetail");
            if(!dir.exists()){
                dir.mkdirs();
            }
            Path folder = Paths.get("/opt/saham/meetingDetail");
            String name = 1+"_"+System.currentTimeMillis();
            Path path = Paths.get(folder.toString(), name+"."+Extension);
            Path out = Files.createFile(path);
            try(InputStream input = file.getInputstream()){
                Files.copy(input, out,StandardCopyOption.REPLACE_EXISTING);
                fileName = name+"."+Extension;
                selected.setFile(fileName);
            }
        }
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle3").getString("MeetingDetailCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() throws IOException {
        String Extension = FilenameUtils.getExtension(file.getFileName());
        if(Extension !=""){
            String fileName = "";
            File dir = new File("/opt/saham/meetingDetail");
            if(!dir.exists()){
                dir.mkdirs();
            }
            if(selected.getFile() != ""){
                File f = new File("/opt/saham/meetingDetail/"+selected.getFile());
                if(f.exists()){
                    f.delete();
                }
            }
            Path folder = Paths.get("/opt/saham/meetingDetail");
            String name = 1+"_"+System.currentTimeMillis();
            Path path = Paths.get(folder.toString(), name+"."+Extension);
            Path out = Files.createFile(path);
            try(InputStream input = file.getInputstream()){
                Files.copy(input, out,StandardCopyOption.REPLACE_EXISTING);
                fileName = name+"."+Extension;
                selected.setFile(fileName);
            }
        }
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle3").getString("MeetingDetailUpdated"));
    }

    public void destroy() {
        if(selected.getFile() !=""){
            File f = new File("/opt/saham/meetingDetail/"+selected.getFile());
            if(f.exists()){
                f.delete();
            }
        }
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle3").getString("MeetingDetailDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<MeetingDetail> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle3").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle3").getString("PersistenceErrorOccured"));
            }
        }
    }

    public MeetingDetail getMeetingDetail(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<MeetingDetail> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<MeetingDetail> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = MeetingDetail.class)
    public static class MeetingDetailControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            MeetingDetailController controller = (MeetingDetailController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "meetingDetailController");
            return controller.getMeetingDetail(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof MeetingDetail) {
                MeetingDetail o = (MeetingDetail) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), MeetingDetail.class.getName()});
                return null;
            }
        }

    }
    
    public String checkPdf(String input){
        String[] r = input.split(".");
        System.out.println(r.length);
        return input;
    }

}

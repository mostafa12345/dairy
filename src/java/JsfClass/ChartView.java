package JsfClass;

import SessionBean.DocumentFacade;
import SessionBean.MessagesFacade;
import SessionBean.SahamFacade;
import javax.annotation.PostConstruct;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;

@ManagedBean
public class ChartView implements Serializable {

    private BarChartModel barModel;
    
    private PieChartModel messagesModel;
    private PieChartModel documentModel;

    public PieChartModel getMessagesModel() {
        return messagesModel;
    }

    public PieChartModel getDocumentModel() {
        return documentModel;
    }
    
    @EJB
    private SahamFacade sahamFacade;
    @EJB
    private MessagesFacade messagesFacade;
    @EJB
    private DocumentFacade documentFacade;
    
    @PostConstruct
    public void init() {
        createBarModels();
        createPieModels();
    }

    private void createPieModels() {
        createMessagesModel();
        createDocumentModel();
    }

    private void createMessagesModel() {
        Long isRead = messagesFacade.getStatus(true);
        
        Long NonRead = messagesFacade.getStatus(false);
        messagesModel = new PieChartModel();
        messagesModel.set("خوانده شده", isRead);
        messagesModel.set("خوانده نشده", NonRead);

        messagesModel.setTitle("وضعیت پیام ها تعداد "+messagesFacade.count());
        messagesModel.setLegendPosition("w");
    }
    
    private void createDocumentModel() {
        Long countActive = documentFacade.getDocumentStatus(true);
        Long countNoneActive = documentFacade.getDocumentStatus(false);
        
        documentModel = new PieChartModel();
        documentModel.set("مدارک تائید شده", countActive);
        documentModel.set("مدارک تائید نشده", countNoneActive);

        documentModel.setTitle("وضعیت مدارک ارسال شده تعداد "+ documentFacade.count());
        documentModel.setLegendPosition("w");
    }

    private BarChartModel initBarModel() {
        BarChartModel model = new BarChartModel();
        int count = sahamFacade.count();
        Long active = sahamFacade.getCountStatus(1),
                activeZo=sahamFacade.getCountStatus(2),
                activeStockTh = sahamFacade.getCountStatus(3),
                activeStockBid = sahamFacade.getCountStatus(4),
                cancelationStoke = sahamFacade.getCountStatus(5),
                cancelationUntitled = sahamFacade.getCountStatus(6)
                ,untitled = sahamFacade.getCountStatus(7);
        
        ChartSeries activeChart = new ChartSeries();
        activeChart.setLabel(
                Integer.valueOf(String.valueOf(Math.round(Float.valueOf(active) / Float.valueOf(count) * 100)))+"% ("+
                "فعال تعداد "+active+")");
        activeChart.set("فعال تعداد "+active, Math.log10(active));
        
        ChartSeries activeZoChart = new ChartSeries();
        activeZoChart.setLabel(
                Integer.valueOf(String.valueOf(Math.round(Float.valueOf(activeZo) / Float.valueOf(count) * 100)))+"% ("+
                "فعال موجود نزد هیئت مدیره تعداد  "+activeZo+")");
        activeZoChart.set("فعال موجود نزد هیئت مدیره", Math.log10(activeZo));
        
        ChartSeries activeStockThChart = new ChartSeries();
        activeStockThChart.setLabel(
                Integer.valueOf(String.valueOf(Math.round(Float.valueOf(activeStockTh) / Float.valueOf(count) * 100)))+"% ("+
                "فعال موجود در تهران تعداد "+activeStockTh+")");
        activeStockThChart.set("فعال موجود در تهران", Math.log10(cancelationStoke));
        
        ChartSeries activeStockBidChart = new ChartSeries();
        activeStockBidChart.setLabel(
                Integer.valueOf(String.valueOf(Math.round(Float.valueOf(activeStockBid) / Float.valueOf(count) * 100)))+"% ("+
                "فعال موجود در کارخانه تعداد "+activeStockBid+")");
        activeStockBidChart.set("فعال موجود در کارخانه", Math.log10(activeStockBid));
        
        ChartSeries cancelationStokeChart = new ChartSeries();
        cancelationStokeChart.setLabel(
                Integer.valueOf(String.valueOf(Math.round(Float.valueOf(cancelationStoke) / Float.valueOf(count) * 100)))+"% ("+
                "ابطالی برگه موجود تعداد "+cancelationStoke+")");
        cancelationStokeChart.set("ابطالی برگه موجود", Math.log10(cancelationStoke));
        
        ChartSeries cancelationUntitedChart = new ChartSeries();
        cancelationUntitedChart.setLabel(
                Integer.valueOf(String.valueOf(Math.round(Float.valueOf(cancelationUntitled) / Float.valueOf(count) * 100)))+"% ("+
                "ابطالی برگه ناموجود تعداد "+cancelationUntitled+")");
        cancelationUntitedChart.set("ابطالی برگه ناموجود", Math.log10(cancelationUntitled));
        
        ChartSeries untitledChart = new ChartSeries();
        untitledChart.setLabel(
                Integer.valueOf(String.valueOf(Math.round(Float.valueOf(untitled) / Float.valueOf(count) * 100)))+"% ("+
                "نامشخص تعداد "+untitled+")");
        untitledChart.set("نامشخص", Math.log10(untitled));

        model.addSeries(activeChart);
        model.addSeries(activeZoChart);
        model.addSeries(activeStockBidChart);
        model.addSeries(activeStockThChart);
        model.addSeries(cancelationStokeChart);
        model.addSeries(cancelationUntitedChart);
        model.addSeries(untitledChart);
        model.setZoom(true);
        model.setAnimate(true);
        return model;
    }

    private void createBarModels() {
        createBarModel();
    }

    public BarChartModel getBarModel() {
        return barModel;
    }

    private void createBarModel() {
        barModel = initBarModel();

        barModel.setTitle("وضعیت سهام ها");
        barModel.setLegendPosition("ne");
        
        Axis xAxis = barModel.getAxis(AxisType.X);
        xAxis.setLabel("مقدار به صورت لگاریتم می باشد");

        Axis yAxis = barModel.getAxis(AxisType.Y);
        //yAxis.setLabel("وضعیت سهام");
        yAxis.setMin(0);
        yAxis.setMax(8);
    }

}

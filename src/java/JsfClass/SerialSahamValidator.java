package JsfClass;

import Entity.Saham;
import SessionBean.SahamFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("SerialValidator")
public class SerialSahamValidator implements Validator {

    @EJB
    private SahamFacade sahamFacade;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        SahamController bean = context.getApplication().evaluateExpressionGet(context, "#{sahamController}", SahamController.class);
        List<Saham> items = bean.getItems();
        for (Saham s : items) {
            if (value.toString().equals(s.getSerialNo())) {
                FacesMessage msg = new FacesMessage("این شماره سریال تکراری می باشد");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        }
    }

}

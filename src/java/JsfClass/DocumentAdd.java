
package JsfClass;

import org.primefaces.model.UploadedFile;


public class DocumentAdd {
    private String desc;
    private UploadedFile file;
    private int order;
    private String label;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public int getOrder() {
        System.out.println("oooorrrrrrdddddddderrrrr iiiiisssssss : " + order);
        return order;
    }

    public DocumentAdd(String desc, UploadedFile file, int order, String label) {
        this.desc = desc;
        this.file = file;
        this.order = order;
        this.label = label;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    
    
}

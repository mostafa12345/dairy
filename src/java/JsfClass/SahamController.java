package JsfClass;

import Entity.Document;
import Entity.Saham;
import Entity.Users;
import JsfClass.util.JsfUtil;
import JsfClass.util.JsfUtil.PersistAction;
import Login.LoginController;
import SessionBean.DocumentFacade;
import SessionBean.SahamFacade;
import SessionBean.UsersFacade;
import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.ULocale;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.sql.DataSource;
import org.apache.commons.io.FilenameUtils;

import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.CroppedImage;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.Visibility;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

@Named("sahamController")
@SessionScoped
public class SahamController implements Serializable {

    @EJB
    private SessionBean.SahamFacade ejbFacade;
    @EJB
    private UsersFacade userFacade;
    private List<Saham> items = null;
    private Saham selected;
    private CroppedImage cimg;
    private String tempfilename;
    private String newFileName;
    private UploadedFile file;
    private List<String> allFile;
    private Document document = new Document();
    @EJB
    private DocumentFacade documentFacade;

    public List<String> getAllFile() {
        if (selected != null && selected.getId() != null) {
            allFile = new ArrayList<>();
//            if (selected.getAllDocument().size() != 0) {
//                for (String s : selected.getAllDocument()) {
//                    allFile.add(s);
//                }
//            }
        }
        return allFile;
    }

    public List<String> getTypeCompany() {
        return ejbFacade.getTypeCompany();
    }

    public List<String> getNCompany() {
        return ejbFacade.getNCompany();
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public CroppedImage getCimg() {
        return cimg;
    }

    public void setCimg(CroppedImage cimg) {
        this.cimg = cimg;
    }

    public String getTempfilename() {
        return tempfilename;
    }

    public void setTempfilename(String tempfilename) {
        this.tempfilename = tempfilename;
    }

    public String getNewFileName() {
        return newFileName;
    }

    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    public void thumbtionalImage(String imgPath, InputStream inf) throws IOException {
        int ORGINAL_HEIGHT = 600;
        int ORGINAL_WIDTH = 600;
        Color BG_COLOR = Color.WHITE;
        Image img = ImageIO.read(inf);
        BufferedImage bi = new BufferedImage(ORGINAL_WIDTH, ORGINAL_HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics g = bi.getGraphics();
        int w = img.getWidth(null);
        int h = img.getHeight(null);
        float r = Float.valueOf(w) / Float.valueOf(h);
        float ORGINAL_RATIO = Float.valueOf(ORGINAL_WIDTH) / Float.valueOf(ORGINAL_HEIGHT);
        g.setColor(BG_COLOR);
        g.fillRect(0, 0, bi.getWidth(), bi.getHeight());

        if (ORGINAL_RATIO > r) {
            int width = Math.round(ORGINAL_HEIGHT * r);
            g.drawImage(img, (ORGINAL_WIDTH - width) / 2, 0, width, ORGINAL_HEIGHT, BG_COLOR, null);
        } else if (ORGINAL_RATIO == r) {
            g.drawImage(img, 0, 0, ORGINAL_WIDTH, ORGINAL_HEIGHT, BG_COLOR, null);
        } else {
            float height = ORGINAL_WIDTH * (Float.valueOf(h) / Float.valueOf(w));
            g.drawImage(img, 0, (ORGINAL_HEIGHT - Math.round(height)) / 2, ORGINAL_WIDTH, Math.round(height), BG_COLOR, null);
        }

        try (FileOutputStream out = new FileOutputStream(imgPath)) {
            ImageIO.write(bi, "jpg", out);
            out.flush();
        }
    }

    public void fileUploadListener(FileUploadEvent event) throws FileNotFoundException, IOException {
        if (event.getFile() != null) {
//            ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
//            ImageWriteParam param = writer.getDefaultWriteParam();
//            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
//            param.setCompressionQuality(0.5f);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            tempfilename = (Math.round(Math.random() * 1000)) + "_" + System.currentTimeMillis() + ".jpg";
            newFileName = externalContext.getRealPath("") + File.separator + "resources" + File.separator + "img"
                    + File.separator + "temp" + File.separator + tempfilename;

            thumbtionalImage(newFileName, event.getFile().getInputstream());

//            ImageOutputStream ios = ImageIO.createImageOutputStream(new FileOutputStream(newFileName));
//            writer.setOutput(ios);
//            writer.write(ImageIO.read(event.getFile().getInputstream()));
        }
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public void createImage() throws IOException, NoSuchAlgorithmException {

        if (cimg != null) {
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(cimg.getBytes()));
            BufferedImage img2 = new BufferedImage(1200, 900, BufferedImage.TYPE_INT_RGB);
            Graphics g = img2.getGraphics();
            g.drawImage(img, 0, 0, 1200, 900, null);
            g.dispose();
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            String dirname = "/opt/saham/document/";
            File dir = new File(dirname);
            if (!dir.exists()) {
                dir.mkdir();
            }
            String newFileName2 = "/opt/saham/document/" + tempfilename;
            ImageIO.write(img2, "jpg", new File(newFileName2));
            if (allFile == null) {
                allFile = new ArrayList<>();
            }
            allFile.add(tempfilename);
            File temp = new File(newFileName);
            if (temp.exists()) {
                temp.delete();
            }
            document.setActive(true);
            document.setFile(tempfilename);
            document.setUserId(selected.getUserId());
            documentFacade.create(document);
        }

        if (!JsfUtil.isValidationFailed()) {
            cimg = null;
            tempfilename = null;
        }
    }

    @Resource(name = "java:app/jdbc/myDatasourceSaham")
    private DataSource ds;
    List<Document> listDocument = new ArrayList<Document>();

    //connect to DB and get customer list
    public List<Document> getDocumentList() throws SQLException {
        listDocument = new ArrayList<>();
        if (selected != null && selected.getUserId() != null) {
            listDocument = documentFacade.getOrder(selected.getUserId());
//            if (ds == null) {
//                throw new SQLException("Can't get data source");
//            }
//
//            //get database connection
//            Connection con = (Connection) ds.getConnection();
//
//            if (con == null) {
//                throw new SQLException("Can't get database connection");
//            }
//
//            PreparedStatement ps
//                    = con.prepareStatement(
//                            "select * from document where user_id = '" + selected.getUserId().getId() + "'");
//
//            //get customer data from database
//            ResultSet result = ps.executeQuery();
//
//            while (result.next()) {
//                Document d = new Document();
//                d.setActive(result.getBoolean("active"));
//                d.setDescribtion(result.getString("describtion"));
//                d.setFile(result.getString("file"));
//                d.setOrder(result.getInt("order"));
//                d.setId(result.getInt("id"));
//                //store all data into a List
//                listDocument.add(d);
//            }

            return listDocument;
        }
        return listDocument;
    }

    public Document order(int order) {
        try {
            getDocumentList();
        } catch (SQLException ex) {
            Logger.getLogger(SahamController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (selected != null) {
            if (selected.getUserId() != null) {
                for (Document s : listDocument) {
                    if (s.getOrder() == order) {
                        return s;
                    }
                }
            }
        }
        return null;
    }

    public void removeImageFile(Document document) {
        if (document != null) {
            documentFacade.remove(document);
        }
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        String pathRemoveFile = "/opt/saham/document/" + document.getFile();

        try {
            File file = new File(pathRemoveFile);
            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }
        } catch (Exception ex) {
            System.err.println(ex.getStackTrace());
        }
    }

    public static void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if (files != null) { //some JVMs return null for empty dirs
            for (File f : files) {
                if (f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }

    public SahamController() {
    }

    public Saham getSelected() {
        return selected;
    }

    public void setSelected(Saham selected) {
        this.selected = selected;
//        if (selected != null) {
//            if (selected.getId() != null) {
//               
//            }
//        }
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private SahamFacade getFacade() {
        return ejbFacade;
    }

    public Saham prepareCreate() {
        selected = new Saham();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        Users user = new Users();
        user.setUsername(selected.getSerialNo());
        Login.LoginController l = new LoginController();
        String pswd = new StringBuilder(String.valueOf(Integer.valueOf(Math.round(Float.valueOf(selected.getSerialNo()))))).reverse().toString();
        user.setPassword(pswd);
        user.setRole("User");
        userFacade.create(user);
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("SahamCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() throws IOException {
        if (file != null) {
            String extension = FilenameUtils.getExtension(file.getFileName());
            if (extension != "") {
                String filename = "";
                File dir = new File("/opt/saham/revoke_document");
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                Path folder = Paths.get("/opt/saham/revoke_document");
                if (selected.getRevokeDocument() != "") {
                    File f = new File("/opt/saham/revoke_document/" + selected.getRevokeDocument());
                    if (f.exists()) {
                        f.delete();
                    }
                }
                extension = FilenameUtils.getExtension(file.getFileName());
                String name = 1 + "_" + System.currentTimeMillis();
                Path path = Paths.get(folder.toString(), name + "." + extension);
                Path outFile = Files.createFile(path);
                try (InputStream input = file.getInputstream()) {
                    Files.copy(input, outFile, StandardCopyOption.REPLACE_EXISTING);
                    filename = name + "." + extension;
                    selected.setRevokeDocument(filename);
                    selected.setRevoke(Short.valueOf("1"));
                    this.file = null;
                }
            }
        }
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("SahamUpdated"));
    }

    public void destroy() throws SQLException {
        for (Document d : getDocumentList()) {
            removeImageFile(d);
        }

        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("SahamDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Saham> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Saham getSaham(java.lang.Integer id) {
        return getFacade().find(id);
    }

    public List<Saham> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Saham> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Saham.class)
    public static class SahamControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            SahamController controller = (SahamController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "sahamController");
            return controller.getSaham(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Saham) {
                Saham o = (Saham) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Saham.class.getName()});
                return null;
            }
        }

    }

    public void postProcessXLS(Object document) throws FileNotFoundException, IOException {
        DataTable dt = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("SahamListForm:datalist");
        String relativeWebPath = "/resources/images/logo2.png";
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        ServletContext servletContext = (ServletContext) externalContext.getContext();
        String absoluteDiskPath = servletContext.getRealPath(relativeWebPath);
        File file = new File(absoluteDiskPath);
        XSSFWorkbook wb = (XSSFWorkbook) document;
        XSSFSheet sheet = wb.getSheetAt(0);
        XSSFFont font = wb.createFont();
        font.setColor(IndexedColors.BLACK.index);
        font.setFontName("B Nazanin");
        font.setFontHeight(12);
        sheet.getCTWorksheet().getSheetViews().getSheetViewArray(0).setRightToLeft(true);
        XSSFRow header = sheet.getRow(1);
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.CORAL.index);
        cellStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        cellStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        cellStyle.setFont(font);

        for (int i = 0; i < header.getPhysicalNumberOfCells(); i++) {
            sheet.autoSizeColumn(i);
            XSSFCell cell = header.getCell(i);
            cell.setCellStyle(cellStyle);
        }

        for (int j = 2; j < sheet.getPhysicalNumberOfRows(); j++) {
            XSSFRow row = sheet.getRow(j);
            for (int i = 0; i < header.getPhysicalNumberOfCells(); i++) {
                XSSFCell cell = row.getCell(i);
                cell.setCellStyle(wb.createCellStyle());
                cell.getCellStyle().setAlignment(XSSFCellStyle.ALIGN_CENTER);
                cell.getCellStyle().setVerticalAlignment(XSSFCellStyle.ALIGN_CENTER);
                cell.getCellStyle().setFont(font);
                cell.getCellStyle().setWrapText(true);
                if (j % 2 == 0) {
                    cell.getCellStyle().setFillForegroundColor(IndexedColors.LIGHT_GREEN.index);
                    cell.getCellStyle().setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
                } else {
                    cell.getCellStyle().setFillForegroundColor(IndexedColors.GREY_25_PERCENT.index);
                    cell.getCellStyle().setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
                }
            }
        }
    }

    private List<Boolean> list = Arrays.asList(
            //     0    1      2    3     4     5     6     7     8      9      10     11     12     13     14     15     16     17     18     19     20      21    22      23     24     25     26     27     28     29     30    31  
            false, true, false, true, false, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true);

    public List<Boolean> getList() {
        return list;
    }

    public void setList(List<Boolean> list) {
        this.list = list;
    }

    public void onToggle(ToggleEvent e) {
        list.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }

    private UploadedFile excel;

    public UploadedFile getExcel() {
        return excel;
    }

    public void setExcel(UploadedFile excel) {
        this.excel = excel;
    }

    public void importExcel() throws IOException {
        String extension = FilenameUtils.getExtension(excel.getFileName());
        if (extension != "") {
            String filename = "";
            File dir = new File("/opt/saham/excel/");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            Path folder = Paths.get("/opt/saham/excel/");
            extension = FilenameUtils.getExtension(excel.getFileName());
            String fileName = 1 + "_" + System.currentTimeMillis();
            Path path = Paths.get(folder.toString(), fileName + "." + extension);
            Path outFile = Files.createFile(path);
            try (InputStream input = excel.getInputstream()) {

                XSSFWorkbook wb = new XSSFWorkbook(input); // Return first sheet from the XLSX workbook 

                HashMap<Integer, String> hmap = new HashMap<Integer, String>();
                List<String> AllName = new ArrayList<>();
                Sheet firstSheet = wb.getSheetAt(0);
                Iterator<Row> iterator = firstSheet.iterator();
                Integer i = 0;
                while (iterator.hasNext()) {
                    Row nextRow = iterator.next();
                    if (i != 0) {
                        String name = "0.0", nameFather = "0.0", serialNo = "0.0", count = "0.0", status = "0.0", nationalCode = "0.0",
                                shSerial = "0.0", shSerialChar = "0.0", shSerialNum = "0.0", shIssued = "0.0",
                                birthDate = "", phone = "0.0", postalCode = "0.0", Address = "0.0";

                        try {
                            name = nextRow.getCell(0).getStringCellValue();
                        } catch (Exception e) {
                            try {
                                name = String.valueOf(nextRow.getCell(0).getNumericCellValue());
                            } catch (Exception e2) {
                            }
                        }
                        try {
                            nameFather = nextRow.getCell(1).getStringCellValue();
                        } catch (Exception e) {
                            try {
                                nameFather = String.valueOf(nextRow.getCell(1).getNumericCellValue());
                            } catch (Exception e2) {
                            }
                        }
                        try {
                            serialNo = String.valueOf(nextRow.getCell(2).getNumericCellValue());
                        } catch (Exception e) {
                            try {
                                serialNo = String.valueOf(nextRow.getCell(2).getStringCellValue());
                            } catch (Exception e2) {
                            }
                        }
                        try {
                            count = String.valueOf(nextRow.getCell(3).getNumericCellValue());
                        } catch (Exception e) {
                            try {
                                count = String.valueOf(nextRow.getCell(3).getStringCellValue());
                            } catch (Exception e2) {
                            }
                        }
                        try {
                            status = String.valueOf(nextRow.getCell(4).getNumericCellValue());
                        } catch (Exception e) {
                            try {
                                status = String.valueOf(nextRow.getCell(4).getStringCellValue());
                            } catch (Exception e2) {
                            }
                        }

                        try {
                            nationalCode = String.valueOf(nextRow.getCell(5).getNumericCellValue());
                        } catch (Exception e) {
                            try {
                                nationalCode = String.valueOf(nextRow.getCell(5).getStringCellValue());
                            } catch (Exception e2) {
                            }
                        }
                        try {
                            shSerial = String.valueOf(nextRow.getCell(6).getNumericCellValue());
                        } catch (Exception e) {
                            try {
                                shSerial = String.valueOf(nextRow.getCell(6).getStringCellValue());
                            } catch (Exception e2) {
                            }
                        }

                        try {
                            shSerialNum = String.valueOf(nextRow.getCell(7).getNumericCellValue());
                        } catch (Exception e) {
                            try {
                                shSerialNum = String.valueOf(nextRow.getCell(7).getStringCellValue());
                            } catch (Exception e2) {
                            }
                        }

                        try {
                            shSerialChar = String.valueOf(nextRow.getCell(8).getNumericCellValue());
                        } catch (Exception e) {
                            try {
                                shSerialChar = String.valueOf(nextRow.getCell(8).getStringCellValue());
                            } catch (Exception e2) {
                            }
                        }
                        try {
                            shIssued = String.valueOf(nextRow.getCell(9).getNumericCellValue());
                        } catch (Exception e) {
                            try {
                                shIssued = String.valueOf(nextRow.getCell(9).getStringCellValue());
                            } catch (Exception e2) {
                            }
                        }

                        try {
                            birthDate = String.valueOf(nextRow.getCell(10).getNumericCellValue());
                        } catch (Exception e) {
                            try {
                                if (!String.valueOf(nextRow.getCell(10).getStringCellValue()).isEmpty()) {
                                    birthDate = String.valueOf(nextRow.getCell(10).getStringCellValue());
                                }
                            } catch (Exception e2) {
                            }
                        }

                        try {
                            phone = String.valueOf(nextRow.getCell(11).getNumericCellValue());
                        } catch (Exception e) {
                            try {
                                phone = String.valueOf(nextRow.getCell(11).getStringCellValue());
                            } catch (Exception e2) {
                            }
                        }
                        try {
                            postalCode = String.valueOf(nextRow.getCell(12).getNumericCellValue());
                        } catch (Exception e) {
                            try {
                                postalCode = String.valueOf(nextRow.getCell(12).getStringCellValue());
                            } catch (Exception e2) {
                            }
                        }

                        System.out.println(name + " ,father is : " + nameFather
                                + " ,seiale is : "
                                + serialNo + " ,count is : " + count + " ,status is : " + status + " ,national code is : "
                                + nationalCode + " ,shSerial is : "
                                + shSerial + " ,shSerialChar is : " + shSerialChar
                                + " ,shSerialNum is : " + shSerialNum
                                + " ,shIssued is : " + shIssued + " ,birthDate is : "
                                + birthDate + " ,phone is : " + phone
                                + " ,postalCode is : " + postalCode
                                + " ,Address is : " + Address);

                        if (hmap.containsValue(name)) {

                            System.out.println("name contain is ==> " + name);
                        }
                        this.selected = new Saham();
                        selected.setSerialNo(String.valueOf(Integer.valueOf(Math.round(Float.valueOf(serialNo)))));
                        selected.setCount(Integer.valueOf(Math.round(Float.valueOf(count))));
                        selected.setStatus(Integer.valueOf(Math.round(Float.valueOf(status))));

                        Users user = new Users();
                        user.setName(name);
                        user.setNameFather(nameFather);
                        if (!nationalCode.equals("0.0")) {
                            user.setActive(true);
                            user.setNationalEconomicalCode(nationalCode);
                            String pswd = new StringBuilder(String.valueOf(Integer.valueOf(Math.round(Float.valueOf(serialNo))))).reverse().toString();
                            Login.LoginController l = new LoginController();
                            pswd = "soltani" + nationalCode;
                            pswd = l.createHash(pswd);
                            user.setPassword(pswd);
                        } else {
                            user.setActive(false);
                        }
                        if (!birthDate.equals("0.0")) {
                            ULocale ul = new ULocale("fa_IR@calender=persian");
                            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd", ul);
                            user.setDateRegisterBirth(df.parse(birthDate));
                        }
                        user.setUsername(String.valueOf(Integer.valueOf(Math.round(Float.valueOf(serialNo)))));
                        user.setRole("User");
                        if (!shIssued.equals("0.0")) {
                            user.setPlaceRegisterBirth(shIssued);
                        }
                        if (!phone.equals("0.0")) {
                            user.setPhone(phone);
                        }
                        if (!postalCode.equals("0.0")) {
                            user.setPostalCode(postalCode);
                        }
                        if (!shSerial.equals("0.0")) {
                            user.setShCode(shSerial);
                        }
                        if (!shSerialNum.equals("0.0")) {
                            user.setShSerialNum(shSerialNum);
                        }
                        if (!shSerialChar.equals("0.0")) {
                            user.setShSerialCh(shSerialChar);
                        }
                        if (!Address.equals("0.0")) {
                            user.setAddress(Address);
                        }
                        if (!hmap.containsValue(name)) {
                            userFacade.create(user);
                            hmap.put(user.getId(), name);
                        } else {
                            Integer id = null;
                            Set set = hmap.entrySet();
                            Iterator iteratorMap = set.iterator();
                            while (iteratorMap.hasNext()) {
                                Map.Entry mentry = (Map.Entry) iteratorMap.next();
                                if (mentry.getValue().equals(name)) {
                                    id = (Integer) mentry.getKey();
                                }
                            }
                            if (id != null) {
                                user = userFacade.find(id);
                            }
                        }
                        String token = UUID.randomUUID().toString();
                        selected.setToken(token);
                        selected.setUserId(user);
                        ejbFacade.create(selected);
                    }
                    i++;
                }
                JsfUtil.addSuccessMessage("اطلاعات با موفقیت ثبت شد");
                wb.close();
                this.excel = null;
            } catch (Exception e) {
                System.err.println(e.getMessage() + "\n" + e.getStackTrace() + "\n" + e.getCause());
                JsfUtil.addSuccessMessage("مشکل در ثبت اطلاعات لطفا اطلاعات خود را دوباره چک کنید  " + e.getMessage());
            }
        }

    }

    private int percent = 0;

    public int getPercent() {
        return percent;
    }

    public int CheckPercent(Saham saham) {
        percent = 0;
//        if (saham.getAddress() != null) {
//            percent += 12;
//        }
//        if (saham.getEmail() != null) {
//            percent += 12;
//        }
//        if (saham.getMobile() != null) {
//            percent += 12;
//        }
//        if (saham.getPhone() != null) {
//            percent += 12;
//        }
//        if (saham.getPostalCode() != null) {
//            percent += 12;
//        }
//        if (saham.getFamilyMfrs() != null) {
//            percent += 12;
//        }
//        if (saham.getName() != null) {
//            percent += 12;
//        }
        if (saham.getCount() != 0) {
            percent += 12;
        }
        return percent;
    }

    public void createDocuments() throws IOException {
        for (DocumentAdd i : listDocumentAdd) {
            String extension = FilenameUtils.getExtension(i.getFile().getFileName());
            if (extension != "") {
                String fileName = "";
                File dir = new File("/opt/saham/document");
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                Path folder = Paths.get("/opt/saham/document");
                String name = 1 + "_" + System.currentTimeMillis();
                Path path = Paths.get(folder.toString(), name + "." + extension);
                Path outFile = Files.createFile(path);
                try (InputStream input = i.getFile().getInputstream()) {
                    Files.copy(input, outFile, StandardCopyOption.REPLACE_EXISTING);
                    fileName = name + "." + extension;
                    document.setFile(fileName);
                }
                document.setActive(true);
                document.setOrder(i.getOrder());
                document.setUserId(selected.getUserId());
                document.setDescribtion(i.getDesc());
                documentFacade.create(document);
            }

            if (!JsfUtil.isValidationFailed()) {
                cimg = null;
                tempfilename = null;
            }
        }
    }

    private List<DocumentAdd> listDocumentAdd = new ArrayList<>(8);

    public List<DocumentAdd> getListDocumentAdd() {
        listDocumentAdd = new ArrayList<>();
        if (this.order(1) == null) {
            listDocumentAdd.add(new DocumentAdd("", file, 1, "روی کارت ملی"));
        }
        if (this.order(2) == null) {
            listDocumentAdd.add(new DocumentAdd("", file, 2, " پشت کارت ملی"));
        }
        if (this.order(3) == null) {
            listDocumentAdd.add(new DocumentAdd("", file, 3, "صفحه اول شناسنامه"));
        }
        if (this.order(4) == null) {
            listDocumentAdd.add(new DocumentAdd("", file, 4, "صفحه دوم شناسنامه"));
        }
        if (this.order(5) == null) {
            listDocumentAdd.add(new DocumentAdd("", file, 5, "صفحه سوم شناسنامه"));
        }
        listDocumentAdd.add(new DocumentAdd("", file, 6, "برگه سهام"));
        listDocumentAdd.add(new DocumentAdd("", file, 7, "فیش واریزی"));
        listDocumentAdd.add(new DocumentAdd("", file, 8, "متفرقه"));
        return listDocumentAdd;
    }

    private List<Integer> percentStatus;

    public List<Integer> getPercentStatus() {
        percentStatus = new ArrayList<>();
        if (percentStatus.isEmpty()) {
            percentStatus.add(0);
            percentStatus.add(0);
            percentStatus.add(0);
            percentStatus.add(0);
            percentStatus.add(0);
            percentStatus.add(0);
            percentStatus.add(0);
            int count = ejbFacade.count();
            for (Saham s : this.getItems()) {
                if (s.getStatus() == 1) {
                    percentStatus.set(0, percentStatus.get(0) + 1);
                }
                if (s.getStatus() == 2) {
                    percentStatus.set(1, percentStatus.get(1) + 1);
                }
                if (s.getStatus() == 3) {
                    percentStatus.set(2, percentStatus.get(2) + 1);
                }
                if (s.getStatus() == 4) {
                    percentStatus.set(3, percentStatus.get(3) + 1);
                }
                if (s.getStatus() == 5) {
                    percentStatus.set(4, percentStatus.get(4) + 1);
                }
                if (s.getStatus() == 6) {
                    percentStatus.set(5, percentStatus.get(5) + 1);
                }
                if (s.getStatus() == 7) {
                    percentStatus.set(6, percentStatus.get(6) + 1);
                }
            }
            percentStatus.set(0, Integer.valueOf(String.valueOf(Math.round(Float.valueOf(percentStatus.get(0)) / Float.valueOf(count) * 100))));
            percentStatus.set(1, Integer.valueOf(String.valueOf(Math.round(Float.valueOf(percentStatus.get(1)) / Float.valueOf(count) * 100))));
            percentStatus.set(2, Integer.valueOf(String.valueOf(Math.round(Float.valueOf(percentStatus.get(2)) / Float.valueOf(count) * 100))));
            percentStatus.set(3, Integer.valueOf(String.valueOf(Math.round(Float.valueOf(percentStatus.get(3)) / Float.valueOf(count) * 100))));
            percentStatus.set(4, Integer.valueOf(String.valueOf(Math.round(Float.valueOf(percentStatus.get(4)) / Float.valueOf(count) * 100))));
            percentStatus.set(5, Integer.valueOf(String.valueOf(Math.round(Float.valueOf(percentStatus.get(5)) / Float.valueOf(count) * 100))));
            percentStatus.set(6, Integer.valueOf(String.valueOf(Math.round(Float.valueOf(percentStatus.get(6)) / Float.valueOf(count) * 100))));
        }
        return percentStatus;
    }

    public void setListDocumentAdd(List<DocumentAdd> listDocumentAdd) {
        this.listDocumentAdd = listDocumentAdd;
    }

}

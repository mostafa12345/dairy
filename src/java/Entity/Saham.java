/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author administrator
 */
@Entity
@Table(name = "saham")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Saham.findAll", query = "SELECT s FROM Saham s"),
    @NamedQuery(name = "Saham.findById", query = "SELECT s FROM Saham s WHERE s.id = :id"),
    @NamedQuery(name = "Saham.findBySerialNo", query = "SELECT s FROM Saham s WHERE s.serialNo = :serialNo"),
    @NamedQuery(name = "Saham.findByCount", query = "SELECT s FROM Saham s WHERE s.count = :count"),})
public class Saham implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "`serial-no`")
    private String serialNo;
    @Column(name = "`date_register`")
    @Temporal(TemporalType.DATE)
    private Date dateRegister;
    @Column(name = "`count`")
    private int count;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    

    @Column(name = "`revoke`")
    private short revoke;
    @Column(name = "`revoke_document`")
    @Size(max = 600)
    private String revokeDocument;
    @Column(name = "`revoke_describtion`")
    @Size(max = 3000)
    private String revokeDescribtion;

    @JoinColumn(name = "user_id")
    @OneToOne
    private Users userId;
    @Column(name = "status")
    private int status;
    @Column(name = "token",length = 60)
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    public Saham() {
    }

    public Saham(Integer id) {
        this.id = id;
    }

    public short getRevoke() {
        return revoke;
    }

    public void setRevoke(short revoke) {
        this.revoke = revoke;
    }

    public String getRevokeDocument() {
        return revokeDocument;
    }

    public void setRevokeDocument(String revokeDocument) {
        this.revokeDocument = revokeDocument;
    }

    public String getRevokeDescribtion() {
        return revokeDescribtion;
    }

    public void setRevokeDescribtion(String revokeDescribtion) {
        this.revokeDescribtion = revokeDescribtion;
    }

    public Saham(Integer id, String serialNo, Date dateRegister, int count) {
        this.id = id;
        this.serialNo = serialNo;
        this.dateRegister = dateRegister;
        this.count = count;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Date getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(Date dateRegister) {
        this.dateRegister = dateRegister;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Saham)) {
            return false;
        }
        Saham other = (Saham) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Saham[ id=" + id + " ]";
    }

    @PreUpdate
    private void PreUpdate() {
        this.updatedAt = new Date();
    }

    @PrePersist
    private void PrePersist() {
        this.createdAt = new Date();
    }
}

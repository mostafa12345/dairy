/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author administrator
 */
@Entity
@Table(name = "meeting_detail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MeetingDetail.findAll", query = "SELECT m FROM MeetingDetail m"),
    @NamedQuery(name = "MeetingDetail.findById", query = "SELECT m FROM MeetingDetail m WHERE m.id = :id"),
    @NamedQuery(name = "MeetingDetail.findByName", query = "SELECT m FROM MeetingDetail m WHERE m.name = :name"),
    @NamedQuery(name = "MeetingDetail.findByFile", query = "SELECT m FROM MeetingDetail m WHERE m.file = :file"),
    @NamedQuery(name = "MeetingDetail.findByDescribtion", query = "SELECT m FROM MeetingDetail m WHERE m.describtion = :describtion"),
    @NamedQuery(name = "MeetingDetail.findByDate", query = "SELECT m FROM MeetingDetail m WHERE m.date = :date"),
    @NamedQuery(name = "MeetingDetail.findByNum", query = "SELECT m FROM MeetingDetail m WHERE m.num = :num"),
    @NamedQuery(name = "MeetingDetail.findByCreatedAt", query = "SELECT m FROM MeetingDetail m WHERE m.createdAt = :createdAt"),
    @NamedQuery(name = "MeetingDetail.findByUpdatedAt", query = "SELECT m FROM MeetingDetail m WHERE m.updatedAt = :updatedAt")})
public class MeetingDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 600)
    @Column(name = "file")
    private String file;
    @Size(max = 4000)
    @Column(name = "describtion")
    private String describtion;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Size(max = 20)
    @Column(name = "num")
    private String num;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    public MeetingDetail() {
    }

    public MeetingDetail(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getDescribtion() {
        return describtion;
    }

    public void setDescribtion(String describtion) {
        this.describtion = describtion;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MeetingDetail)) {
            return false;
        }
        MeetingDetail other = (MeetingDetail) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.MeetingDetail[ id=" + id + " ]";
    }
    
    @PreUpdate
    public void PreUpdate(){
        this.updatedAt = new Date();
    }
    
    @PrePersist
    public void PrePersist(){
        this.createdAt = new Date();
    }
    
}

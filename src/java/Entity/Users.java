/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author administrator
 */
@Entity
@Table(name = "users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u"),
    @NamedQuery(name = "Users.findById", query = "SELECT u FROM Users u WHERE u.id = :id"),
    @NamedQuery(name = "Users.findByGroupname", query = "SELECT u FROM Users u WHERE u.groupname = :groupname"),
    @NamedQuery(name = "Users.findByLastlogin", query = "SELECT u FROM Users u WHERE u.lastlogin = :lastlogin"),
    @NamedQuery(name = "Users.findByPassword", query = "SELECT u FROM Users u WHERE u.password = :password"),
    @NamedQuery(name = "Users.findByRole", query = "SELECT u FROM Users u WHERE u.role = :role"),
    @NamedQuery(name = "Users.findByUsername", query = "SELECT u FROM Users u WHERE u.username = :username"),
    @NamedQuery(name = "Users.findByCreatedAt", query = "SELECT u FROM Users u WHERE u.createdAt = :createdAt"),
    @NamedQuery(name = "Users.findBySearchPerson", query = "SELECT s FROM Users s WHERE s.familyMfrs = :familyMfrs or s.nationalEconomicalCode = :nationalEconomicalCode or s.shSerialNum = :shSerialNum"),
    @NamedQuery(name = "Users.findByUpdatedAt", query = "SELECT u FROM Users u WHERE u.updatedAt = :updatedAt")})
public class Users implements Serializable {

    @OneToMany(mappedBy = "userId")
    private Collection<Messages> messagesCollection;
    @OneToMany(mappedBy = "userIdAdmin")
    private Collection<Messages> messagesCollection1;
    @OneToMany(mappedBy = "userId")
    private Collection<Document> documentCollection;

    public Collection<Document> getDocumentCollection() {
        return documentCollection;
    }

    public void setDocumentCollection(Collection<Document> documentCollection) {
        this.documentCollection = documentCollection;
    }

    @OneToMany(mappedBy = "userId")
    private List<Saham> saham;

    public List<Saham> getSaham() {
        return saham;
    }

    public void setSaham(List<Saham> saham) {
        this.saham = saham;
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 255)
    @Column(name = "GROUPNAME")
    private String groupname;
    @Column(name = "LASTLOGIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastlogin;
    @Size(max = 255)
    @Column(name = "password")
    private String password;
    @Size(max = 255)
    @Column(name = "role")
    private String role;
    @Size(max = 255)
    @Column(name = "username")
    private String username;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Basic(optional = false)
    @Size(max = 400)
    @Column(name = "family_mfrs")
    private String familyMfrs;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Column(name = "sex")
    private Short sex;
    @Size(max = 255)
    @Column(name = "name_father")
    private String nameFather;
    @Size(max = 40)
    @Column(name = "sh_code")
    private String shCode;
    @Size(max = 40)
    @Column(name = "sh_serial_num")
    private String shSerialNum;
    @Size(max = 40)
    @Column(name = "sh_serial_ch")
    private String shSerialCh;
    @Basic(optional = false)
    @Column(name = "date_register_birth")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateRegisterBirth;
    @Column(name = "place_register_birth")
    @Size(max = 400)
    private String placeRegisterBirth;
    @Basic(optional = false)
    @Size(max = 50)
    @Column(name = "national_economical_code")
    private String nationalEconomicalCode;
    @Basic(optional = false)
    @Size(max = 30000)
    @Column(name = "`address`")
    private String address;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @Size(max = 30000)
    @Column(name = "`phone`")
    private String phone;
    @Basic(optional = false)
    @Size(max = 30000)
    @Column(name = "`mobile`")
    private String mobile;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @Size(max = 300)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Size(max = 50)
    @Column(name = "postal_code")
    private String postalCode;
    @Basic(optional = false)
    @Column(name = "is_company")
    private short isCompany;
    @Size(max = 400)
    @Column(name = "organization_type")
    private String organizationType;
    @Size(max = 400)
    @Column(name = "organization_nature")
    private String organizationNature;
    @Size(max = 250)
    @Column(name = "registration_number")
    private String registrationNumber;
    @Size(max = 255)
    @Column(name = "sh_issued")
    private String shIssued;
    @Column(name = "active")
    private boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getShIssued() {
        return shIssued;
    }

    public void setShIssued(String shIssued) {
        this.shIssued = shIssued;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyMfrs() {
        return familyMfrs;
    }

    public void setFamilyMfrs(String familyMfrs) {
        this.familyMfrs = familyMfrs;
    }

    public Short getSex() {
        return sex;
    }

    public void setSex(Short sex) {
        this.sex = sex;
    }

    public String getNameFather() {
        return nameFather;
    }

    public void setNameFather(String nameFather) {
        this.nameFather = nameFather;
    }

    public String getShCode() {
        return shCode;
    }

    public void setShCode(String shCode) {
        this.shCode = shCode;
    }

    public String getShSerialNum() {
        return shSerialNum;
    }

    public void setShSerialNum(String shSerialNum) {
        this.shSerialNum = shSerialNum;
    }

    public String getShSerialCh() {
        return shSerialCh;
    }

    public void setShSerialCh(String shSerialCh) {
        this.shSerialCh = shSerialCh;
    }

    public Date getDateRegisterBirth() {
        return dateRegisterBirth;
    }

    public void setDateRegisterBirth(Date dateRegisterBirth) {
        this.dateRegisterBirth = dateRegisterBirth;
    }

    public String getPlaceRegisterBirth() {
        return placeRegisterBirth;
    }

    public void setPlaceRegisterBirth(String placeRegisterBirth) {
        this.placeRegisterBirth = placeRegisterBirth;
    }

    public String getNationalEconomicalCode() {
        return nationalEconomicalCode;
    }

    public void setNationalEconomicalCode(String nationalEconomicalCode) {
        this.nationalEconomicalCode = nationalEconomicalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public short getIsCompany() {
        return isCompany;
    }

    public void setIsCompany(short isCompany) {
        this.isCompany = isCompany;
    }

    public String getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(String organizationType) {
        this.organizationType = organizationType;
    }

    public String getOrganizationNature() {
        return organizationNature;
    }

    public void setOrganizationNature(String organizationNature) {
        this.organizationNature = organizationNature;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @ManyToOne
    private Province province;
    @ManyToOne
    private City city;

    public Users() {
    }

    public Users(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public Date getLastlogin() {
        return lastlogin;
    }

    public void setLastlogin(Date lastlogin) {
        this.lastlogin = lastlogin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Users[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<Messages> getMessagesCollection() {
        return messagesCollection;
    }

    public void setMessagesCollection(Collection<Messages> messagesCollection) {
        this.messagesCollection = messagesCollection;
    }

    @XmlTransient
    public Collection<Messages> getMessagesCollection1() {
        return messagesCollection1;
    }

    public void setMessagesCollection1(Collection<Messages> messagesCollection1) {
        this.messagesCollection1 = messagesCollection1;
    }

    @PrePersist
    private void PrePersist() {
        this.createdAt = new Date();
    }

    @PreUpdate
    private void PreUpdate() {
        this.updatedAt = new Date();
    }
}

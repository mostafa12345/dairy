/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author administrator
 */
@Entity
@Table(name = "period")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Period.findAll", query = "SELECT p FROM Period p"),
    @NamedQuery(name = "Period.findById", query = "SELECT p FROM Period p WHERE p.id = :id"),
    @NamedQuery(name = "Period.findByPrice", query = "SELECT p FROM Period p WHERE p.price = :price"),
    @NamedQuery(name = "Period.findByStartDate", query = "SELECT p FROM Period p WHERE p.startDate = :startDate"),
    @NamedQuery(name = "Period.findByEndDate", query = "SELECT p FROM Period p WHERE p.endDate = :endDate"),
    @NamedQuery(name = "Period.findByIsBase", query = "SELECT p FROM Period p WHERE p.isBase = :isBase"),
    @NamedQuery(name = "Period.findByIsNow", query = "SELECT p FROM Period p WHERE p.isNow = :isNow"),
    @NamedQuery(name = "Period.findByDescribtion", query = "SELECT p FROM Period p WHERE p.describtion = :describtion"),
    @NamedQuery(name = "Period.findByCreatedAt", query = "SELECT p FROM Period p WHERE p.createdAt = :createdAt"),
    @NamedQuery(name = "Period.findByUpdatedAt", query = "SELECT p FROM Period p WHERE p.updatedAt = :updatedAt")})
public class Period implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    private long price;
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_base")
    private boolean isBase;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_now")
    private boolean isNow;
    @Size(max = 3000)
    @Column(name = "describtion")
    private String describtion;
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Column(name="shares_offer")
    private float sharesOffer;
    @Column(name = "old_count")
    private Integer oldCount;
    @Column(name = "new_count")
    private Integer newCount;
    @Column(name = "last_serial_no")
    private Integer lastSerialNo;
    @Column(name = "not_equale")
    private Integer notEquale;

    public Integer getNotEquale() {
        return notEquale;
    }

    public void setNotEquale(Integer notEquale) {
        this.notEquale = notEquale;
    }
    public Integer getLastSerialNo() {
        return lastSerialNo;
    }

    public void setLastSerialNo(Integer lastSerialNo) {
        this.lastSerialNo = lastSerialNo;
    }
    public float getSharesOffer() {
        return sharesOffer;
    }

    public void setSharesOffer(float sharesOffer) {
        this.sharesOffer = sharesOffer;
    }

    public Integer getOldCount() {
        return oldCount;
    }

    public void setOldCount(Integer oldCount) {
        this.oldCount = oldCount;
    }

    public Integer getNewCount() {
        return newCount;
    }

    public void setNewCount(Integer newCount) {
        this.newCount = newCount;
    }
    
    public Period() {
    }

    public Period(Integer id) {
        this.id = id;
    }

    public Period(Integer id, long price, Date startDate, Date endDate, boolean isBase, boolean isNow) {
        this.id = id;
        this.price = price;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isBase = isBase;
        this.isNow = isNow;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean getIsBase() {
        return isBase;
    }

    public void setIsBase(boolean isBase) {
        this.isBase = isBase;
    }

    public boolean getIsNow() {
        return isNow;
    }

    public void setIsNow(boolean isNow) {
        this.isNow = isNow;
    }

    public String getDescribtion() {
        return describtion;
    }

    public void setDescribtion(String describtion) {
        this.describtion = describtion;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Period)) {
            return false;
        }
        Period other = (Period) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Period[ id=" + id + " ]";
    }
    
    @PreUpdate
    private void PreUpdate(){
        this.updatedAt = new Date();
    }
    @PrePersist
    private void PrePersist(){
        this.createdAt = new Date();
    }
}

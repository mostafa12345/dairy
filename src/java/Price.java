/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Client.UserPanelController;
import Entity.Period;
import Entity.Saham;
import SessionBean.PeriodFacade;
import com.ibm.icu.text.NumberFormat;
import com.ibm.icu.util.ULocale;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author darash
 */
@WebServlet(name = "Price", urlPatterns = {"/Price"}, initParams = {
    @WebInitParam(name = "value", value = "0")})
public class Price extends HttpServlet {
    
    @EJB
    private PeriodFacade periodFacade;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, FontFormatException {
        response.setContentType("text/html;charset=UTF-8");
        String bg = request.getRealPath("") + File.separator + "resources" + File.separator + "images" + File.separator + "saham.jpg";
        Period base = periodFacade.getNow();
        BufferedImage img = ImageIO.read(new File(bg));
        Graphics2D g = (Graphics2D) img.getGraphics();
        AffineTransform orig = g.getTransform();
        File fontUrl = new File(request.getRealPath("") + File.separator + "resources" + File.separator + "css" + File.separator + "font" + File.separator + "BYekan.ttf");
        Font font = Font.createFont(Font.TRUETYPE_FONT, fontUrl);
        font = font.deriveFont(Font.PLAIN, 30);
        GraphicsEnvironment ge
                = GraphicsEnvironment.getLocalGraphicsEnvironment();
        ge.registerFont(font);
        g.setFont(font);
        g.setColor(Color.BLACK);
        ULocale ul = new ULocale("fa", "IR");
        Integer total = 2500;
        NumberFormat format = NumberFormat.getInstance(ul);
        Saham saham = new Saham();
        try{
         saham = (Saham) request.getSession().getAttribute("sahamSelected");
        }catch(Exception e){}
        //Count
        g.drawString(String.valueOf(saham.getCount()), 870, 450);
        
        //Serial No
        g.drawString(saham.getSerialNo(), 210 , 327);
        
        //price now period
        g.drawString(String.valueOf(periodFacade.getNow().getPrice()), 210, 457);
        
        //price num
        g.drawString(String.valueOf(saham.getCount() * periodFacade.getNow().getPrice()), 848, 525);
        
        //price char
        ConvertToCharPersion numTOchar = new ConvertToCharPersion();
        font = font.deriveFont(Font.PLAIN, 22);
        g.setFont(font);
        g.drawString(numTOchar.num2str(String.valueOf(saham.getCount() * periodFacade.getNow().getPrice())), 340, 500);
        
        //Name 
        font = font.deriveFont(Font.PLAIN, 30);
        g.setFont(font);
        g.drawString(saham.getUserId().getName()+" "+(saham.getUserId().getFamilyMfrs() != null ? saham.getUserId().getFamilyMfrs() : ""), 220, 595);
        System.out.println(saham.getUserId().getFamilyMfrs());
        
        //Name Father or manager
        g.drawString(saham.getUserId().getNameFather(), 900, 665);
        
        //national Code or Register code
        g.drawString(saham.getUserId().getNationalEconomicalCode(), 250, 665);
        
        g.setTransform(orig);
        g.dispose();
        OutputStream os = response.getOutputStream();
        ImageIO.write(img, "jpg", os);
        os.close();
       
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (FontFormatException ex) {
            Logger.getLogger(Price.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (FontFormatException ex) {
            Logger.getLogger(Price.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

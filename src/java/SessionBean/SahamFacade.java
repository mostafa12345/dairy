/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBean;

import Entity.Saham;
import java.util.Iterator;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author administrator
 */
@Stateless
public class SahamFacade extends AbstractFacade<Saham> {

    @PersistenceContext(unitName = "sahamPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SahamFacade() {
        super(Saham.class);
    }

    public List<String> getTypeCompany() {
        return em.createQuery("SELECT s.organizationType FROM Users s WHERE s.organizationType IS NOT NULL GROUP BY s.organizationType ORDER BY s.organizationType").getResultList();
    }

    public List<String> getNCompany() {
        return em.createQuery("SELECT s.organizationNature FROM Users s WHERE s.organizationType IS NOT NULL GROUP BY s.organizationNature ORDER BY s.organizationNature").getResultList();
    }

    public List<Saham> getSearchPerson(String family, String nationalCode, String shCodeNum) {
        return em.createNamedQuery("Users.findBySearchPerson")
                .setParameter("familyMfrs", family)
                .setParameter("nationalEconomicalCode", nationalCode)
                .setParameter("shSerialNum", shCodeNum)
                .getResultList();
    }

    public Long getCountStatus(int type) {
        return (Long) em.createQuery("select count(s)  from Saham s where s.status = :status").setParameter("status", type).setMaxResults(1).getSingleResult();
    }

    public Integer getCountActiveSaham() {
        List<Integer> c = em.createQuery(
                "select s.count from Saham s where s.status = :status1 or s.status=:status2"
        ).setParameter("status1", 1)
                .setParameter("status2", 2)
                .getResultList();
        Integer count = 0;
        for (Integer i : c) {
            count = count + i;
        }
        return count;
    }
    
    public Saham getLastSerialNo(){
     return (Saham) em.createQuery("select s from Saham s order by s.serialNo*1 DESC").setMaxResults(1).getSingleResult();
    }
    }

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBean;

import Entity.Period;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.PersistenceContext;

/**
 *
 * @author administrator
 */
@Stateless
public class PeriodFacade extends AbstractFacade<Period> {

    @PersistenceContext(unitName = "sahamPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PeriodFacade() {
        super(Period.class);
    }

    public Period getBase() {
        try {
            return (Period) em.createNamedQuery("Period.findByIsBase").setParameter("isBase", true).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    public Period getNow() {
        try {
            return (Period) em.createNamedQuery("Period.findByIsNow").setParameter("isNow", true).getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    public int setNewNows() {
        int result = em.createQuery("update Period p set p.isNow = :isNow where p.isNow = :isType")
                .setParameter("isNow", false)
                .setParameter("isType", true)
                .executeUpdate();
        return result;
    }

    public int setNewBases() {
        int result = em.createQuery("update Period p set p.isBase = :isBase where p.isBase = :isType")
                .setParameter("isBase", false)
                .setParameter("isType", true)
                .executeUpdate();
        return result;
    }
}

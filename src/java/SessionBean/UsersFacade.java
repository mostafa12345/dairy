/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBean;

import Entity.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author administrator
 */
@Stateless
public class UsersFacade extends AbstractFacade<Users> {

    @PersistenceContext(unitName = "sahamPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsersFacade() {
        super(Users.class);
    }

    public List<String> getTypeCompany() {
        return em.createQuery("SELECT s.organizationType FROM Users s WHERE s.organizationType IS NOT NULL GROUP BY s.organizationType ORDER BY s.organizationType").getResultList();
    }

    public List<String> getNCompany() {
        return em.createQuery("SELECT s.organizationNature FROM Users s WHERE s.organizationType IS NOT NULL GROUP BY s.organizationNature ORDER BY s.organizationNature").getResultList();
    }
    
    public Users getByName(String name){
      return (Users) em.createQuery("select u from Users u where u.name=:name").setParameter("name", name).setMaxResults(1).getSingleResult();
    }
}

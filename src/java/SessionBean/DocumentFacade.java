/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBean;

import Entity.Document;
import Entity.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author administrator
 */
@Stateless
public class DocumentFacade extends AbstractFacade<Document> {

    @PersistenceContext(unitName = "sahamPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DocumentFacade() {
        super(Document.class);
    }

    public List<Document> getOrder(Users user) {
        try {
            return (List<Document>)em.createNamedQuery("Document.findByUser").setParameter("userId", user).getResultList();
        } catch (Exception e) {
            return null;
        }
    }
    
    public Long getDocumentStatus(boolean type){
        return (Long) em.createQuery("select count(d) from Document d where d.active=:active").setParameter("active", type).setMaxResults(1).getSingleResult();
    }

}

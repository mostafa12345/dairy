/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import Entity.Saham;
import JsfClass.util.JsfUtil;
import SessionBean.SahamFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@RequestScoped
public class Search {
    private Saham saham;
    @EJB
    private SahamFacade sahamFacade;
    
    private boolean isCompany ;

    private String family,nationalCode,ShCodeNum,economicalCode,registerCode;
    
    public boolean isIsCompany() {
        return isCompany;
    }

    public void setIsCompany(boolean isCompany) {
        this.isCompany = isCompany;
    }
    
    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public String getShCodeNum() {
        return ShCodeNum;
    }

    public void setShCodeNum(String ShCodeNum) {
        this.ShCodeNum = ShCodeNum;
    }

    public String getEconomicalCode() {
        return economicalCode;
    }

    public void setEconomicalCode(String economicalCode) {
        this.economicalCode = economicalCode;
    }

    public String getRegisterCode() {
        return registerCode;
    }

    public void setRegisterCode(String registerCode) {
        this.registerCode = registerCode;
    }
    
    public void person(){
        List<Saham> s = sahamFacade.getSearchPerson(family, nationalCode, ShCodeNum);
        int i = 0;
        for(Saham se : s ){
            i++;
//            System.out.println("Result serach "+i+" ,name is : "+se.getName()+" ,family is : "+family);
        }
    }
}

package Client;

import Entity.Document;
import Entity.Messages;
import Entity.Period;
import Entity.Saham;
import Entity.Users;
import JsfClass.util.JsfUtil;
import Login.LoginController;
import SessionBean.DocumentFacade;
import SessionBean.MessagesFacade;
import SessionBean.PeriodFacade;
import SessionBean.SahamFacade;
import SessionBean.UsersFacade;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.jms.Message;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.poifs.crypt.HashAlgorithm;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.chart.PieChartModel;

@ManagedBean(name = "UserPanel")
public class UserPanelController {

    private Users user = (Users) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
    private Period base;
    private Period now;
    @EJB
    private PeriodFacade periodFacade;
    @EJB
    private DocumentFacade documentFacade;

    private List<Document> documents = new ArrayList<>();

    private UploadedFile file;

    public int roundNumber(float num) {
        return Math.round(num);
    }

    public Period getBase() {
        this.base = periodFacade.getBase();
        return base;
    }

    public Period getNow() {
        this.now = periodFacade.getNow();
        return now;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public List<Document> getDocuments() {
        return (List<Document>) user.getDocumentCollection();
    }

    public Users getUser() {
        return user;
    }

    public Document order(int order) {
        List<Document> l = documentFacade.getOrder(user);
        if (!l.isEmpty()) {
            for (Document s : l) {
                if (s.getOrder() == order) {
                    return s;
                }
            }
        }
        return null;
    }

    private Document newDocument = new Document();

    public Document getNewDocument() {
        return newDocument;
    }

    public void setNewDocument(Document newDocument) {
        this.newDocument = newDocument;
    }

    public void prepareCreate() {
        this.newDocument = new Document();
    }

    public void create() throws IOException {
        String extension = FilenameUtils.getExtension(file.getFileName());
        if (extension != "") {
            String fileName = "";
            File dir = new File("/opt/saham/document");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            Path folder = Paths.get("/opt/saham/document");
            String name = 1 + "_" + System.currentTimeMillis();
            Path path = Paths.get(folder.toString(), name + "." + extension);
            Path outFile = Files.createFile(path);
            try (InputStream input = file.getInputstream()) {
                Files.copy(input, outFile, StandardCopyOption.REPLACE_EXISTING);
                fileName = name + "." + extension;
                newDocument.setFile(fileName);
                this.file = null;
            }
            newDocument.setActive(false);
            newDocument.setUserId(user);
            newDocument.setStatus(1);
            documentFacade.create(newDocument);
            JsfUtil.addSuccessMessage("اطلاعات با موفقیت ثبت شد");
        }
        JsfUtil.addErrorMessage("مشکل در ثبت اطلاعات لطفا اطلاعات را بررسی و مجددا ارسال نمائید");
    }

    public void setUser(Users user) {
        this.user = user;
    }
    @EJB
    private SahamFacade sahamFacade;
    private String pswd1 = "";
    private String pswd2 = "";

    public String getPswd1() {
        return pswd1;
    }

    public void setPswd1(String pswd1) {
        this.pswd1 = pswd1;
    }

    public String getPswd2() {
        return pswd2;
    }

    public void setPswd2(String pswd2) {
        this.pswd2 = pswd2;
    }

    public void updateSaham() {
//        sahamFacade.edit(user.getSaham());
        JsfUtil.addSuccessMessage("اطلاعات با موفقیت ویرایش شد");
    }
    @EJB
    private UsersFacade userFacade;

    public boolean updateProfile() throws NoSuchAlgorithmException, IOException {
        if (!pswd1.equals(pswd2)) {
            JsfUtil.addErrorMessage("رمز عبور وارد شده یکی نمی باشد");
            return false;
        }
        if (user.getUsername() == "") {
            JsfUtil.addErrorMessage("نام کاربری نمی تواند خالی باشد");
            return false;
        }
        Login.LoginController l = new LoginController();
        pswd1 = l.createHash(pswd1);
        user.setPassword(pswd1);
        userFacade.edit(user);
        JsfUtil.addSuccessMessage("اطلاعات شما با موفقیت ویرایش شد");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
        FacesContext.getCurrentInstance().getExternalContext().redirect(
                "/dairy/login/login.xhtml?redirect=true");
        return true;
    }
    private Messages message = new Messages();
    @EJB
    private MessagesFacade messagesFacade;
    @EJB
    private UsersFacade usersFacade;

    public Messages getMessage() {
        return message;
    }

    public void setMessage(Messages message) {
        this.message = message;
    }

    public boolean createMessage() {
        if (message.getMessage() == "" || message.getMessage().equals(null)) {
            JsfUtil.addErrorMessage("متن پیام نمی تواند خالی باشد");
            return false;
        }
        message.setUserId(user);
        message.setMessageType(2);
        message.setIsRead(false);
        messagesFacade.create(message);
        JsfUtil.addSuccessMessage("پیام با موفقیت ارسال شد");
        this.user = usersFacade.find(user.getId());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
        return true;
    }

    private Messages selectMessage = new Messages();

    public Messages getSelectMessage() {
        return selectMessage;
    }

    public void setSelectMessage(Messages selectMessage) {
        this.selectMessage = selectMessage;
    }

    public void messageRead() {
        if (selectMessage.getMessageType().equals(2)) {
            selectMessage.setIsRead(true);
            messagesFacade.edit(selectMessage);
        }
    }

    ////Pie Chart
    private PieChartModel percent;

    public PieChartModel getPercent() {
        percent = new PieChartModel();

        percent.set("Brand 1", 540);
        percent.set("Brand 2", 325);
        percent.set("Brand 3", 702);
        percent.set("Brand 4", 421);

        percent.setTitle("Simple Pie");
        percent.setLegendPosition("w");
        return percent;
    }

    public void setPercent(PieChartModel percent) {
        this.percent = percent;
    }

    private static final long serialVersionUID = 20120316L;
    private String renderMethod;
    private String text;
    private String label;
    private int mode;
    private int size;
    private String fillColor;

    public UserPanelController() {
        renderMethod = "image";
        label = "SD";
        mode = 2;
        fillColor = "7d767d";
        size = 130;
    }

    public String getRenderMethod() {
        return renderMethod;
    }

    public void setRenderMethod(String renderMethod) {
        this.renderMethod = renderMethod;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getFillColor() {
        return fillColor;
    }

    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    private Saham sahamSelected;

    public Saham getSahamSelected() {
        return sahamSelected;
    }

    public void setSahamSelected(Saham sahamSelected) throws NoSuchAlgorithmException {
        this.sahamSelected = sahamSelected;
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("sahamSelected", sahamSelected);
        this.text = createHash(sahamSelected.getToken() + sahamSelected.getSerialNo());
        System.out.println(createHash(sahamSelected.getToken() + sahamSelected.getSerialNo()));
        this.label = "SD" + sahamSelected.getSerialNo();
    }

    public String createHash(String input) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(input.getBytes(), 0, input.length());
        return new BigInteger(1, m.digest()).toString(16);
    }

}

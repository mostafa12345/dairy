/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import Entity.Contact;
import JsfClass.util.JsfUtil;
import SessionBean.ContactFacade;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

@ManagedBean(name = "main")
@RequestScoped
public class Main {
    private Contact contact = new Contact();
    @EJB
    private ContactFacade contactFacade;
    
    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public void create(){
        contactFacade.create(contact);
        JsfUtil.addSuccessMessage("اطلاعات با موفقیت ثبت شد");
    }
    
}

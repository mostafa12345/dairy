// Input Lock
var data = {
    email: {
        success: false,
        msg: 'لطفا ایمیل را به درستی وارد کنید',
        value: ''
    },
    mobile: {
        success: false,
        msg: 'لطفا تلفن همراه را به درستی وارد کنید',
        value: ''
    },
    name: {
        success: false,
        msg: 'لطفا نام خود را وارد کنید',
        value: ''
    },
    msg: {
        success: false,
        msg: 'لطفا پیام خود را وارد کنید',
        value: ''
    }
};

$('textarea').blur(function () {
    $('#hire textarea').each(function () {
        $this = $(this);
        if (this.value != '') {
            data.msg.success = true;
            data.msg.value = this.value;
            $this.addClass('focused');
            $('textarea + label + span').css({'opacity': 1});
        } else {
            data.msg.success = false;
            $this.removeClass('focused');
            $('textarea + label + span').css({'opacity': 0});
        }
    });
});

$("input[name*='name']").blur(function () {
    $("input[name*='name']").each(function () {
        $this = $(this);
        if (this.value != '') {
            data.name.success = true;
            data.name.value = this.value;
            $this.addClass('focused');
            $("#span1").css({'opacity': 1});
        } else {
            data.name.success = false;
            $this.removeClass('focused');
        }
    });
});

$("input[name*='email']").blur(function () {
    $("input[name*='email']").each(function () {
        $this = $(this);
        if (this.value != '') {
            if (validateEmail(this.value)) {
                data.email.success = true;
                data.email.value = this.value;
                $this.addClass('focused');
                $("#span2").css({'opacity': 1});
            } else {
                data.email.success = false;
            }
        } else {
            data.email = false;
            $this.removeClass('focused');
        }
    });
});

$("input[name*='mobile']").blur(function () {
    $("input[name*='mobile']").each(function () {
        $this = $(this);
        if (this.value != '') {
            if (validateMobile(this.value)) {
                data.mobile.success = true;
                data.mobile.value = this.value;
                $this.addClass('focused');
                $("#span3").css({'opacity': 1});
            } else {
                data.mobile.success = false;
            }
        } else {
            data.mobile = false;
            $this.removeClass('focused');
        }
    });
});

//$(document).on('submit', '.ajax-form', function () {
//    var error = false;
//    var msg = "";
//    for (var s in data) {
//        if (data[s]['success'] == false) {
//            msg += "<br>" + data[s]['msg'];
//            error = true;
//        }
//    }
//    $('#ErrorBox').html(msg);
//    if(error == false){
//        return true;
//    }
//    return false;
//});

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validateMobile(mobile) {
    var re = /^09[1-3][0-9]{8}$/;
    return re.test(mobile);
}